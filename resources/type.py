import logging
from flasgger import swag_from
from flask import request
from models.type import TypeModel
from flask_restful import Resource, reqparse
from datetime import datetime


from utils import paginated_results, restrict

class Type(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('description', type = str)
    parser.add_argument('name', type = str)
    parser.add_argument('creation_date', type=lambda x: datetime.strptime(x, '%Y-%m-%d').date())


    #@swag_from('../swagger/type/get_type.yaml')
    def get(self, id):
        type = TypeModel.find_by_id(id)
        if type:
            return type.json()

        return {'message': 'No se encuentra el tipo'}, 404

    #@swag_from('../swagger/type/put_type.yaml')
    def put(self, id):
        type = TypeModel.find_by_id(id)
        if type:
            newdata = Type.parser.parse_args()
            type.from_reqparse(newdata)
            type.save_to_db()
            return type.json()

        return {'message': 'No se encuentra el tipo'}, 404
    
    #@swag_from('../swagger/type/delete_type.yaml')
    def delete(self, id):
        task = TypeModel.find_by_id(id)
        if task:
            task.delete_from_db()

        return {'message': 'Se ha borrado el tipo'}
        
class TypeList(Resource):
    #@swag_from('../swagger/type/list_type.yaml')
    def get(self):
        query = TypeModel.query
        return paginated_results(query)

    #@swag_from('../swagger/type/post_type.yaml')
    def post(self):
        data = Type.parser.parse_args()

        id = data.get('id')

        if id is not None and TypeModel.find_by_id(id):
            return {'message': "Ya existe un tipo con el id"}, 404
        
        type = TypeModel(**data)
        try:
            type.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error el tipo.', exc_info=e)
            return {'message': "Ocurrio un error al crear el tipo"}, 500
        return type.json(), 201

class TypeSearch(Resource):
    #@swag_from('../swagger/type/search_type.yaml')
    def post(self):
        query = TypeModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id', lambda x: TypeModel.id == x)
            query = restrict (query, filters,'description', lambda x: TypeModel.description.contains(x))
            query = restrict (query, filters,'name', lambda x: TypeModel.name == x)
            query = restrict (query, filters,'creation_date', lambda x: TypeModel.creation_date == x)
        return paginated_results(query)