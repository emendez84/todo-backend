import logging
from flasgger import swag_from
from flask import request
from models.task import TaskModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Task(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('description', type = str)
    parser.add_argument('status', type = str)
    parser.add_argument('id_type', type = int)


    @swag_from('../swagger/task/get_task.yaml')
    def get(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            return task.json()

        return {'message': 'No se encuentra la Task'}, 404

    @swag_from('../swagger/task/put_task.yaml')
    def put(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            newdata = Task.parser.parse_args()
            task.from_reqparse(newdata)
            task.save_to_db()
            return task.json()

        return {'message': 'No se encuentra la Task'}, 404
    
    @swag_from('../swagger/task/delete_task.yaml')
    def delete(self, id):
        task = TaskModel.find_by_id(id)
        if task:
            task.delete_from_db()

        return {'message': 'Se ha borrado Task'}
        
class TaskList(Resource):
    @swag_from('../swagger/task/list_task.yaml')
    def get(self):
        query = TaskModel.query
        return paginated_results(query)

    @swag_from('../swagger/task/post_task.yaml')
    def post(self):
        data = Task.parser.parse_args()

        id = data.get('id')

        if id is not None and TaskModel.find_by_id(id):
            return {'message': "Ya existe una tarea con el id"}, 404
        
        task = TaskModel(**data)
        try:
            task.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear Tarea.', exc_info=e)
            return {'message': "Ocurrio un error al crear la Tarea"}, 500
        return task.json(), 201

class TaskSearch(Resource):
    @swag_from('../swagger/task/search_task.yaml')
    def post(self):
        query = TaskModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id', lambda x: TaskModel.id == x)
            query = restrict (query, filters,'description', lambda x: TaskModel.description.contains(x))
            query = restrict (query, filters,'status', lambda x: TaskModel.status == x)
            query = restrict (query, filters,'id_type', lambda x: TaskModel.id_type == x)
        return paginated_results(query)